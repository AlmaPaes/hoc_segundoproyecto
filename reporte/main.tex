\documentclass{article}
\textheight=19cm
\textwidth=14cm
\topmargin=-2cm
\oddsidemargin = 1cm
\usepackage{amsmath,amssymb,amsfonts,latexsym,textcomp}
\usepackage[table]{xcolor} %agregar color a la tabla
\usepackage{graphicx,caption,subcaption}
%\graphicspath{ {./Imagenes/} }
\usepackage{float}
\restylefloat{table}
%fecha en español
\usepackage[spanish]{babel}
\usepackage[utf8]{inputenc}
\usepackage[left = 2.5cm, bottom = 2cm, top=2cm]{geometry}
\usepackage{tcolorbox}
\usepackage{boldline}
\usepackage{longtable}

\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=cyan,    
    urlcolor=magenta,
    citecolor=lilblue
}

\definecolor{lightmauve}{rgb}{0.86, 0.82, 1.0}
\definecolor{coralpink}{rgb}{0.97, 0.51, 0.47}
\definecolor{lightcarminepink}{rgb}{0.9, 0.4, 0.38}
\definecolor{airforceblue}{rgb}{0.36, 0.54, 0.66}
\definecolor{lilblue}{rgb}{0.152,0.441,0.718}

\usepackage{fancyhdr}
 
\pagestyle{fancy}
\fancyhf{}
\rhead{Páes Alcalá Alma Rosa}
\lhead{Segundo Proyecto: Generalized Assignment Problem}


\begin{document}
\title{
  {\sc Universidad Nacional Autónoma de México}\\
  {\sc Facultad de Ciencias}\\
  {\sc Seminario de Heurísticas y Optimización Combinatoria 2021-1}\\
  {\sc Reporte de Proyecto 2: Generalaized Assignment Problem}\\
}
\author{Páes Alcalá Alma Rosa}
%fecha
\date{\today}
\maketitle

\section*{Problema a resolver}

\noindent
El Problema de Asignación Generalizado (conocido por sus siglas GAP\footnote{Proveniente del nombre original \emph{Generalized Assignment Problem}}) proviene del objetivo de asignar \emph{n} tareas a \emph{m} trabajadores. Basándonos en el trabajo de Kundakcioglu y Alizamir (2008)\cite{GAP}, establecemos que esta asignación presenta las siguientes propiedades: 

\begin{itemize}
    \item Cada trabajador tiene cierta capacidad para hacer tareas. Esta capacidad es una cota superior, y no debe ser rebasada. Podemos verla como la función:
    \[
    b: M \rightarrow \mathbb{R}, \; b_m = e
    \], 
    donde M es el conjunto de trabajadores.
    \item El asignar una tarea a un trabajador lleva consigo el esfuerzo de un trabajador, y este esfuerzo se refleja en un valor. 
    \[
    a: N \times M \rightarrow \mathbb{R}, \; a_{ij}= d
    \]
    ,donde N es el conjunto de tareas, M es el conjunto de trabajadores, y $i \in N$, $j \in M$.
    Es de esperarse que entre mayor sea el esfuerzo del trabajador por realizar esta tarea, menos deseable es que le sea asignada a éste.
    \item La asignación de una tarea a un trabajador conlleva un costo.
    \[
    c: N \times M \rightarrow \mathbb{R}, \; c_{ij}= c
    \]
    ,donde N es el conjunto de tareas, M es el conjunto de trabajadores, y $i \in N$, $j \in M$.
    Entre menos cueste la realización de una actividad, mejor.
\end{itemize}

\noindent
Entendiendo el problema, se busca minimizar el siguiente valor:
\[
\sum_{i=1}^m \sum_{j=1}^n c_{ij}*x_{ij}
\], donde $c_{ij}$ es el costo de que un trabajador realice una tarea, y $x$ se define como sigue:

\[
x =
\left\{
    \begin{array}{ll}
        1 & \text{si la tarea i es asignada al trabajador j} \\
        0 & \text{en otro caso}
    \end{array}
\right.
\]

\noindent
Existen variaciones de este problema, como la presentada en Martello y Toth(1990)\cite{MINGAP}, en donde la función de optimización no es el costo, sino el beneficio de que un trabajador realice una cierta tarea. Por lo tanto, se convierte en un problema de maximización.


Se dice que una solución es \emph{factible} si cumple con las siguientes restricciones: 

\begin{itemize}
    \item Ningún trabajador debe sobrepasar su capacidad.
    \[
    \sum_{i=1}^n a_{ij}*x_{ij} \leq b_j, \; j \in \{1,...,m\}
    \]
    
    \item Cada tarea sólo puede estar asignada a exactamente a un trabajador. No puede haber tareas sin asignar.
    \[
    \sum_{j=1}^m x_{ij} = 1, \; i \in \{1,...,n\}
    \]
\end{itemize}



\section*{Heurística}

 \noindent
 La heurística a utilizar se llama Optimización de las Aves Viajeras (MBO, por sus siglas en inglés\footnote{Migrating Birds Optimization}), propuesta inicialmente por Duman, Uysal y Alkaya (2012)\cite{MBO}. Es una metaheurística basada en la formación de vuelos de muchas aves al viajar, la cual se asemeja a una "V" mayúscula.\\
 
 \begin{figure}[H]
    \centering
    \includegraphics[scale=0.6]{birds.jpg}
    \caption{Vuelo de las aves}
\end{figure}
 
 En esta formación, existe una ave líder, y se sitúa al frente (en la punta de la "V"); y existen dos filas de aves a cada lado de ella. Los aleteos de la ave principal al volar causan vórtices de aire, que son aprovechados por las aves en sus flancos izquierdo y derecho, y gastan menos energía que la ave principal. Esta ayuda es proporcionada por todas los miembros, así que un ave siempre se beneficia del que está frente a él.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.6]{vortice_bonito.jpg}
    \caption{Vórtices creados por la ave principal, y que aprovechan las aves traseras para hacer menos esfuerzo al volar}
\end{figure}

\noindent
Después de cierto tiempo, el líder es el que ha gastado más energía, y es entonces relevado por alguna otra ave. De esta manera, los pájaros pueden volar mayores distancias gastando menos energía. \\

A partir de este comportamiento, se definió una metaheurística que aprovecha los principios en los que se basa este vuelo para la resolución de problemas. 

MBO es una técnica de búsqueda local. Comienza con un cierto número de soluciones iniciales, que serían las aves en la formación "V". Cada solución genera una cierta cantidad de vecinos (esta cantidad depende de si es el lider, o uno de los secundarios); y si uno de los vecinos es mejor que el actual, entonces éste es reemplazado. Además, cada uno de los ejemplares que no son líderes se benefician de los vecinos no utilizados de las soluciones anteiores a él, así que hay mayor probabilidad de mejora.\\

El diagrama de flujo que representa a este procedimiento lo vemos aquí:

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.5]{mbo_flujo.png}
    \caption{Flujo de MBO}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.6]{formation_mbo.png}
    \caption{Formación bajo los parámetros n=5, k=3, x=1}
    \label{fig:my_label}
\end{figure}

La ejecución y desempeño de ésta depende de varios factores: 

\begin{itemize}
    \item La cantidad de soluciones iniciales (\textbf{n}). Vista como la cantidad de aves en la formación. Una cantidad muy pequeña es propensa a que sus soluciones se queden atoradas en mínimos locales; y una cantidad grande exploraría demasiado del espacio de soluciones, y por lo tanto, ocuparía mucho tiempo en obtener una solución.
    
    \item El número de soluciones vecinas que genera la solución líder (\textbf{k}). Puede ser interpretada como la fuerza inducida requerida por el vuelo, que es inversamente proporcional a la velocidad de avance. Por lo tanto, se recomiendan valores bajos.
    
    \item El número de soluciones vecinas que serán compartidas con la siguiente solución (\textbf{x}). Es considerada la distancia en la que se traslapan dos puntas de las alas de las aves (o WTS, por sus siglas en inglés \footnote{Wing-Tip Spacing}).
    
    \begin{figure}[H]
        \centering
        \includegraphics{wts.jpg}
        \caption{Wing-Tip Space}
    \end{figure}
    
    Este parámetro es la representación de la ventaja de vuelo que las aves traseras reciben de las soluciones frente a ellas. Un valor muy alto de \textbf{x}, en la heurística, significa que todas las soluciones se parecerán entre sí, y puede llegar a la convergencia prematura.
    
    \item Número de tours (\textbf{m}). Es la cantidad de tiempo (o iteraciones) en las que la posición actual de las aves se mantendrá, antes de que el líder actual sea renovado por otro, y éste pase a las filas traseras. Buscamos valores modelados de este parámetro, para aprovechar la solución lider sin caer en mínimos locales o en una falta de convergencia.
    
    \item Número de iteraciones en total (\textbf{K}). Visto como el tiempo total de vuelo.
\end{itemize}

\noindent
A continuación se presentan detalles de la implementación de esta heurística.

\section*{Implementación}

\noindent
Para resolver el problema GAP con la metaheurística MBO se utilizó el lenguaje Rust, y se dividió el proceso de resolución en cinco módulos:

\begin{enumerate}
    \item \textbf{Módulo principal (main)}: Parametrización y ejecución de la heurística.
    \item \textbf{Lectura de la instancia (database)}:  Lectura de la instancia a través de una base de datos.
    \item \textbf{Compilación de datos (data)}: Estructura que guarda los datos recolectados de la base de datos.
    \item \textbf{Modelado de solución (solucion)}: Creación de una estructura que guarda un clon de la instancia, la asignación de tareas a trabajadores -que viene siendo la solución representada-, su costo, y la cantidad de trabajadores que sobrepasan su capacidad. Puede generar un vecino suyo.
    \item \textbf{Heurística (migracion\_aves)}: Implementación de la heurística MBO.
\end{enumerate}

\noindent
Se explican a continuación algunos detalles de la implementación de la heurística que modifican algunas de las consideraciones del problema GAP, con el fin de llegar a una solución más eficientemente, o por conveniencia de código.


\subsection*{Penalizaciones}

\noindent
Sea $M$ el conjunto de trabajadores de nuestra instancia y $N$ el número de tareas a asignar, definimos el \textbf{número de penalizaciones de una solución} \emph{p} de la siguiente manera:

\[
p = \vert \{ j \in M \; | \; \sum_{i=1}^{|N|} a_{ij}*x_{ij} > b_j\} \vert
\]

\noindent
En palabras, es el número de trabajadores cuyas tareas asignadas sobrepasan su capacidad de trabajo. Trivialmente, aseguramos que una solución factible tiene cero penalizaciones.

\subsection*{Función de costo}

\noindent
Para evitar que soluciones no factibles tengan menor costo que las soluciones factibles, y asegurar que el resultado final sea una solución factible -a menos que no haya encontrado alguna-, modificamos la función de costo; de manera que el objetivo sea minimizar la cifra: 

\[
\sum_{i=1}^m \sum_{j=1}^n c_{ij} * x_{ij} * (p+1)
\]

\noindent
donde $p$ es el número de penalizaciones de la solución. Si una solución tiene a un trabajador sobrecargado, el costo real (definido en el planteamiento del problema) será multiplicado por dos; si tiene a dos trabajadores sobrecargados, el costo real será multiplicado por tres; etc. Así, las soluciones no factibles pierden prioridad en la búsqueda de soluciones.

\subsection*{Vecino de una solución}

\noindent
Sea $u$ una solución, decimos que tiene un vecino $v$ si difieren exactamente en la asignación de una tarea.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.38]{vecinos.png}
    \caption{Dos soluciones vecinas}
\end{figure}

La elección de trabajadores que harán la transferencia de una tarea es hecha aleatoriamente, salvo ciertas restricciones: 

\begin{itemize}
    \item Por obviedad, no se le pueden quitar tareas a un trabajador al que no se le ha asignado alguna.
    \item El trabajador que recibe la tarea no debe estar sobrecargado al momento de hacer la transferencia.
\end{itemize}


\section*{Mejores resultados}

\subsection*{Instancia 1}

\noindent
La instancia uno consta de mil (1000) tareas que deben ser asignadas a quinientos (500) trabajadores. El mejor resultado encontrado tiene las siguientes características:

\begin{table}[H]
    \centering
    \begin{tabular}{ll}
        \textbf{Factible} & Sí\\
        \textbf{Costo} & 4602.1277\\
        \textbf{Número de soluciones iniciales (n)} & 91\\
        \textbf{Número de soluciones vecinas para mejorar al líder (k)} & 11\\
        \textbf{Número de soluciones compartidas con las soluciones siguientes (x)} & 4\\
        \textbf{Número de tours (m)} & 30\\
        \textbf{Número de iteraciones (K)} & $400^3 = 64,000,000$\\
        \textbf{Semilla} & 1\\
        \textbf{Tiempo aproximado de ejecución} & 1,333.7 segundos $\approx$ 23 minutos\\
    \end{tabular}
\end{table}

\begin{table}[H]
    \centering
    \begin{tabular}{llV{6}ll}
        \textbf{Trabajador} & \textbf{Tareas asignadas} & \textbf{Trabajador} & \textbf{Tareas asignadas}\\
        1 & 200 & 2 & $\emptyset$ \\
        3 & 876 & 4 & 918, 125 \\
        5 & 930 & 6 & 467, 758, 15 \\
        7 & 400 & 8 & 326, 378 \\
        9 & 213, 468, 133 & 10 & 875, 252 \\
        11 & 272, 648, 142, 39 & 12 & 551, 377, 426 \\
        13 & 993 & 14 & 222, 916, 238 \\
        15 & 505, 539, 440 & 16 & 775, 121, 623, 772, 46, 91 \\
        17 & $\emptyset$ & 18 & 939 \\
        19 & $\emptyset$ & 20 & 591, 810, 188, 600, 7 \\
        21 & 767 & 22 & 845, 391 \\
        23 & $\emptyset$ & 24 & $\emptyset$\\
        25 & 171, 618, 300 & 26 & 695, 548, 781, 26 \\
        27 & 141, 808, 53, 74, 104 & 28 & $\emptyset$ \\
        29 & 560, 704, 902 & 30 & 432 \\
        31 & $\emptyset$ & 32 & 482, 260, 982 \\
        33 & 444 & 34 & 920 \\
        35 & 368, 259, 394, 757 & 36 & 856 \\
        37 & $\emptyset$ & 38 & 9 \\
        39 & 333 & 40 & $\emptyset$ \\
        41 & 353 & 42 & $\emptyset$ \\
        43 & $\emptyset$ & 44 & 100 \\
        45 & 665, 321 & 46 & 807, 830 \\
        47 & 575, 204, 712, 360 & 48 & 6, 951, 766, 524, 81, 947 \\
        49 & 90, 507, 233 & 50 & $\emptyset$ \\
    \end{tabular}
    \caption{Mejor solución para la instancia 1 (tabla 1)}
\end{table}

\begin{table}[H]
    \centering
    \begin{tabular}{llV{6}ll}
        \textbf{Trabajador} & \textbf{Tareas asignadas} & \textbf{Trabajador} & \textbf{Tareas asignadas}\\
        51 & 666, 317 & 52 & 962, 979 \\
        53 & 130 & 54 & 382 \\
        55 & $\emptyset$ & 56 & 411 \\
        57 & 660, 258 & 58 & 786 \\
        59 & 242 & 60 & 379, 966, 752 \\
        61 & 682, 642, 308 & 62 & 34, 725, 266, 518, 887, 825, 789 \\
        63 & $\emptyset$ & 64  \\
        65 & 149 & 66 & $\emptyset$ \\
        67 & $\emptyset$ & 68 & 957, 745, 124, 25, 461, 346, 399 \\
        69 & $\emptyset$ & 70 & 101, 525 \\
        71 & 721, 287, 144, 943 & 72 & 77 \\
        73 & 386 & 74 & 886 \\
        75 & 330, 476 & 76 & 888, 451, 366 \\
        77 & 319 & 78 & 946, 209, 594, 607 \\
        79 & 874, 678 & 80 & 597 \\
        81 & 14 & 82 & 692 \\
        83 & $\emptyset$ & 84 & 674, 655, 180, 257 \\
        85 & 464 & 86 & 861, 652, 370, 873, 369, 309, 380 \\
        87 & 801 & 88 & 595 \\
        89 & 61, 831 & 90 & 92, 187, 956, 362, 427, 455 \\
        91 & 694, 458 & 92  \\
        93 & $\emptyset$ & 94 & 192 \\
        95 & 768, 984, 102, 421 & 96 & 32, 244 \\
        97 & 128, 714, 58 & 98 & 867, 351, 536, 698, 158, 511, 686, 166 \\
        99 & 664, 885 & 100 & 31 \\
        101 & 952 & 102 & 475 \\
        103 & 3, 93 & 104 & 900 \\
        105 & 568 & 106 & 255, 555, 645 \\
        107 & 603, 428, 8 & 108 & 152, 170 \\
        109 & $\emptyset$ & 110 & $\emptyset$  \\
        111 & $\emptyset$ & 112 & $\emptyset$ \\
        113 & 824, 574, 479, 55, 931, 815 & 114 & 342, 154 \\
        115 & 406, 672, 923, 955, 236 & 116 & 117, 713, 334 \\
        117 & 1000, 562, 415 & 118 & 263, 859, 441 \\
        119 & 442, 579 & 120 & 277 \\
        121 & 521 & 122 & 868, 139, 779 \\
        123 & 239, 347, 62 & 124 & 763, 41, 519, 611 \\
        125 & 747, 653, 115, 485 & 126 & 289 \\
        127 & 927 & 128 & 375, 390, 870 \\
        129 & 439, 959 & 130 & 926, 598 \\
        131 & 832 & 132 & 895 \\
        133 & 43, 822, 416 & 134 & 726, 976 \\
        135 & 820 & 136 & 320, 65 \\
        137 & 82, 431, 535 & 138 & 478 \\
        139 & $\emptyset$ & 140 & $\emptyset$ \\
        141 & 561, 140 & 142 & $\emptyset$ \\
        143 & 903, 155 & 144 & 160, 693, 457, 491 \\
        145 & 857 & 146 & 419, 205, 676 \\
        147 & $\emptyset$ & 148 & 894, 40, 48 \\
        149 & 571, 851, 541, 842, 36, 484 & 150 & 780 \\
    \end{tabular}
    \caption{Mejor solución para la instancia 1 (tabla 2)}
\end{table}

\begin{table}[H]
    \centering
    \begin{tabular}{llV{6}ll}
        \textbf{Trabajador} & \textbf{Tareas asignadas} & \textbf{Trabajador} & \textbf{Tareas asignadas}\\
        151 & $\emptyset$ & 152  \\
        153 & 630, 610, 24, 165 & 154 & 70 \\
        155 & 498, 528 & 156 & 945, 649, 632 \\
        157 & 52, 599, 743 & 158 & 396, 195, 492 \\
        159 & $\emptyset$ & 160 & 119 \\
        161 & 924, 530, 27 & 162 & 425, 928, 572, 651, 818, 299 \\
        163 & 804 & 164  \\
        165 & 453, 473, 569 & 166 & 146 \\
        167 & 977, 953, 463 & 168 & 671, 294 \\
        169 & 450, 385 & 170 & $\emptyset$ \\
        171 & 490, 740, 16 & 172 & 626 \\
        173 & 84 & 174 & 989 \\
        175 & 944, 656, 899 & 176 & 604, 332 \\
        177 & 352 & 178 & $\emptyset$ \\
        179 & 677, 746 & 180 & 72 \\
        181 & 617, 429, 462 & 182 & 145, 413, 423, 22 \\
        183 & 554, 123 & 184 & 710, 47, 577 \\
        185 & 878 & 186 & 110 \\
        187 & 542, 418, 410 & 188 & 310, 118 \\
        189 & 877, 589, 79 & 190 & 397, 76, 18 \\
        191 & 612 & 192 & 293, 540, 404 \\
        193 & 929, 276, 356, 761 & 194 & 340, 108 \\
        195 & 269 & 196 & $\emptyset$ \\
        197 & 322, 667, 249, 99 & 198 & 764 \\
        199 & 970 & 200 & $\emptyset$ \\
        201 & 219, 472, 184, 113 & 202 & $\emptyset$ \\
        203 & 735, 749, 253, 565, 742 & 204 & 958, 120 \\
        205 & 657, 496, 297 & 206 & 723 \\
        207 & $\emptyset$ & 208 & $\emptyset$ \\
        209 & 355 & 210 & $\emptyset$ \\
        211 & $\emptyset$ & 212 & 284, 802, 234 \\
        213 & 636, 403, 241, 459, 986, 737 & 214 & 637, 85 \\
        215 & 950 & 216 & $\emptyset$ \\
        217 & $\emptyset$ & 218 & 908, 627, 274, 683 \\
        219 & 348, 722 & 220 & 669, 207, 756 \\
        221 & 620 & 222 & 54, 566 \\
        223 & $\emptyset$ & 224 & 151 \\
        225 & 848, 194, 999, 422 & 226 & $\emptyset$ \\
        227 & 583, 230 & 228 & 138, 474, 860, 307, 134 \\
        229 & 919, 570, 543, 673, 759, 111 & 230 & 97, 893, 514, 387 \\
        231 & 56 & 232 & 997, 305, 883, 844, 960, 596 \\
        233 & $\emptyset$ & 234 & 275 \\
        235 & 817 & 236 & 254, 167, 50 \\
        237 & 483 & 238 & 371, 223, 787, 392, 303, 531, 983, 304 \\
        239 & $\emptyset$ & 240 & 573, 446 \\
        241 & 470, 803, 534 & 242 & 663, 433 \\
        243 & 556, 83 & 244 & $\emptyset$ \\
        245 & 587, 190, 821, 522 & 246 & 865, 217 \\
        247 & 788, 754, 163, 286, 972, 605 & 248 & 235, 28, 819 \\
        249 & 925 & 250 & $\emptyset$ \\
    \end{tabular}
    \caption{Mejor solución para la instancia 1 (tabla 3)}
\end{table}

\begin{table}[H]
    \centering
    \begin{tabular}{llV{6}ll}
        \textbf{Trabajador} & \textbf{Tareas asignadas} & \textbf{Trabajador} & \textbf{Tareas asignadas}\\
        251 & 127, 447, 793 & 252 & 901, 412 \\
        253 & 183, 658, 323 & 254 & 736, 343, 816, 179, 697, 724, 350 \\
        255 & 116, 389 & 256 & 843 \\
        257 & 5, 739, 538, 466, 727 & 258 & 137 \\
        259 & $\emptyset$ & 260 & 985 \\
        261 & 480, 537 & 262 & 448, 94, 78, 216 \\
        263 & 750, 730, 707 & 264 & 220 \\
        265 & $\emptyset$ & 266 & $\emptyset$ \\
        267 & $\emptyset$ & 268 & $\emptyset$ \\
        269 & 214, 438, 189, 19 & 270 & 437, 504, 177 \\
        271 & 331 & 272 & 292, 913 \\
        273 & 500, 619, 203 & 274 & 316 \\
        275 & 517, 954 & 276 & 823, 805, 581 \\
        277 & 872, 335, 654 & 278 & 398 \\
        279 & $\emptyset$ & 280 & 486, 839, 367 \\
        281 & 21, 776 & 282 & 800 \\
        283 & 687 & 284 & 243 \\
        285 & $\emptyset$ & 286 & 358, 921, 601 \\
        287 & 98, 226, 914, 325, 992 & 288 & $\emptyset$ \\
        289 & 51, 454, 168, 580, 227, 311 & 290 & 731 \\
        291 & 414, 344, 829, 80, 285 & 292 & 176, 732, 11, 974, 891 \\
        293 & 282, 33, 281 & 294 & 835, 940 \\
        295 & 20, 998, 585, 95, 132 & 296 & 744, 172 \\
        297 & $\emptyset$ & 298 & 489, 813, 136, 769 \\
        299 & 852 & 300 & 313, 584, 381, 773 \\
        301 & 592 & 302 & $\emptyset$ \\
        303 & $\emptyset$ & 304 & 755, 328, 508 \\
        305 & 679, 675 & 306 & 245, 430, 212 \\
        307 & 75 & 308 & 164 \\
        309 & 858, 910, 512, 38 & 310 & 264, 456, 278 \\
        311 & $\emptyset$ & 312 & 290, 696 \\
        313 & 268 & 314 & 135 \\
        315 & 302 & 316 & 624, 402, 912 \\
        317 & $\emptyset$ & 318 & 975, 703 \\
        319 & 978 & 320 & $\emptyset$ \\
        321 & $\emptyset$ & 322 & 973, 760 \\
        323 & 89, 606, 668, 407 & 324 & 374, 559, 621 \\
        325 & 10, 838, 59 & 326 & $\emptyset$ \\
        327 & 564, 211 & 328 & $\emptyset$ \\
        329 & 641, 625 & 330 & 563, 17, 156 \\
        331 & 338, 552 & 332 & 711, 29, 516, 937 \\
        333 & 778, 558, 198, 221 & 334 & 193, 644 \\
        335 & 785, 880 & 336 & 546, 783, 938, 613, 639 \\
        337 & 685, 388 & 338 & 273, 532 \\
        339 & 201 & 340 & 45, 981, 395 \\
        341 & 376 & 342 & $\emptyset$ \\
        343 & $\emptyset$ & 344 & 60 \\
        345 & 634, 995, 295 & 346 & $\emptyset$ \\
        347 & 409 & 348 & 12 \\
        349 & 684, 354, 963 & 350 & $\emptyset$ \\
    \end{tabular}
    \caption{Mejor solución para la instancia 1 (tabla 4)}
\end{table}

\begin{table}[H]
    \centering
    \begin{tabular}{llV{6}ll}
        \textbf{Trabajador} & \textbf{Tareas asignadas} & \textbf{Trabajador} & \textbf{Tareas asignadas}\\
        351 & 814 & 352 & 906 \\
        353 & $\emptyset$ & 354 & $\emptyset$ \\
        355 & 879, 544, 49, 905 & 356 & 229, 306 \\
        357 & 169, 148 & 358 & 582 \\
        359 & 846, 794 & 360 & 738, 529, 527, 942, 578 \\
        361 & 364 & 362 & 106 \\
        363 & 866, 495, 526, 909 & 364 & 405, 393, 373 \\
        365 & 701, 622 & 366 & 841 \\
        367 & $\emptyset$ & 368 & 681, 336, 834, 502 \\
        369 & 853, 481 & 370 & 948, 225 \\
        371 & 199, 996, 702, 88 & 372 & 365, 762, 256 \\
        373 & 855, 147, 729 & 374 & $\emptyset$ \\
        375 & 708, 161, 646 & 376 & 248 \\
        377 & 345, 296, 915, 357, 424 & 378 & 638, 469, 774, 716, 836, 57, 64 \\
        379 & 941, 628 & 380 & 864 \\
        381 & 363, 635 & 382 & 670, 840 \\
        383 & 847, 791, 795, 417, 907 & 384 & 849, 488, 215 \\
        385 & $\emptyset$ & 386 & 533, 706, 105 \\
        387 & $\emptyset$ & 388 & 932, 232, 202, 71, 897 \\
        389 & 733, 359, 240, 777 & 390 & 615, 224, 809, 503, 329 \\
        391 & 608, 153, 753, 965, 662 & 392 & 523, 549 \\
        393 & 372, 884, 882, 661 & 394 & 35, 68 \\
        395 & 159, 13, 107 & 396 & 477, 717, 279, 150, 312, 339, 798, 811, 23 \\
        397 & 862, 797 & 398 & 103, 237 \\
        399 & 315 & 400 & 792, 283 \\
        401 & 949, 896 & 402 & 465 \\
        403 & 122, 709, 680, 131, 460 & 404 & $\emptyset$ \\
        405 & 251 & 406 & 715, 44, 4, 688 \\
        407 & 96, 112, 231 & 408 & 494, 206, 705, 812, 129 \\
        409 & $\emptyset$ & 410 & 741 \\
        411 & 547 & 412 & 445, 420 \\
        413 & $\emptyset$ & 414 & 969 \\
        415 & 401, 631 & 416 & 471, 828, 434 \\
        417 & $\emptyset$ & 418 & $\emptyset$ \\
        419 & 265, 854, 314, 126 & 420 & $\emptyset$ \\
        421 & 850, 593, 640 & 422 & 162 \\
        423 & 37, 826 & 424 & 633, 301, 650, 917, 863, 270, 66 \\
        425 & $\emptyset$ & 426 & 384, 2, 197 \\
        427 & $\emptyset$ & 428 & 782, 497, 383 \\
        429 & 250, 690 & 430 & $\emptyset$\\
        431 & 191 & 432 & 837, 493, 748 \\
        433 & $\emptyset$ & 434 & 443, 298 \\
        435 & 557, 553, 728, 881 & 436 & $\emptyset$ \\
        437 & 182, 291, 936, 157 & 438 & 765, 967, 806 \\
        439 & 602, 318, 186 & 440 & $\emptyset$ \\
        441 & 934, 487 & 442 & 228, 961, 971 \\
        443 & 30, 550, 143, 349, 341 & 444 & 185 \\
        445 & 175, 218, 616, 513, 452 & 446 & 114 \\
        447 & 588 & 448 & 990, 271 \\
        449 & 647 & 450 & $\emptyset$ \\
    \end{tabular}
    \caption{Mejor solución para la instancia 1 (tabla 5)}
\end{table}


\begin{table}[H]
    \centering
    \begin{tabular}{llV{6}ll}
        \textbf{Trabajador} & \textbf{Tareas asignadas} & \textbf{Trabajador} & \textbf{Tareas asignadas}\\
        451 & 991, 770, 210 & 452 & 988, 435, 987, 796, 181 \\
        453 & $\emptyset$ & 454 & 499, 700 \\
        455 & 790, 890 & 456 & 892 \\
        457 & 799 & 458 & $\emptyset$ \\
        459 & 720 & 460 & 659, 288 \\
        461 & $\emptyset$ & 462 & $\emptyset$ \\
        463 & 510 & 464 & 267 \\
        465 & 699, 643 & 466 & $\emptyset$ \\
        467 & 178, 173, 629, 327 & 468 & 689, 337, 691 \\
        469 & 515, 247, 545 & 470 & $\emptyset$ \\
        471 & 904 & 472 & 506, 933 \\
        473 & 771 & 474 & $\emptyset$ \\
        475 & 871, 261, 833 & 476 & $\emptyset$ \\
        477 & 1, 898 & 478 & 262, 63 \\
        479 & 576 & 480 & 196 \\
        481 & 911 & 482 & 509 \\
        483 & 590, 449, 324, 208, 408 & 484 & 586, 567 \\
        485 & 174 & 486 & 889, 827, 109, 87, 361, 968, 73, 246 \\
        487 & 42, 67, 751, 280 & 488 & $\emptyset$ \\
        489 & $\emptyset$ & 490 & 734, 869, 922 \\
        491 & 614 & 492 & $\emptyset$ \\
        493 & 436, 501, 609 & 494 & $\emptyset$ \\
        495 & 964, 719, 718 & 496 & $\emptyset$ \\
        497 & 520, 935 & 498 & 784, 994 \\
        499 & $\emptyset$ & 500 & 69, 86, 980 \\
    \end{tabular}
    \caption{Mejor solución para la instancia 1 (tabla 6)}
\end{table}

\subsection*{Instancia 2}

\noindent
La instancia dos consta de doscientas (200) tareas que deben ser asignadas a setenta y cinco (75) trabajadores. El mejor resultado encontrado tiene las siguientes características:

\begin{table}[H]
    \centering
    \begin{tabular}{ll}
        \textbf{Factible} & Sí\\
        \textbf{Costo} & 3030.0920\\
        \textbf{Número de soluciones iniciales (n)} & 91\\
        \textbf{Número de soluciones vecinas para mejorar al líder (k)} & 15\\
        \textbf{Número de soluciones compartidas con las soluciones siguientes (x)} & 4\\
        \textbf{Número de tours (m)} & 30\\
        \textbf{Número de iteraciones (K)} & $250*245*240 = 15,000,000$\\
        \textbf{Semilla} & 8\\
        \textbf{Tiempo aproximado de ejecución} & 55 segundos\\
    \end{tabular}
\end{table}

\begin{table}[H]
    \centering
    \begin{tabular}{llV{6}ll}
        \textbf{Trabajador} & \textbf{Tareas asignadas} & \textbf{Trabajador} & \textbf{Tareas asignadas}\\
        1 & 65, 70, 199 & 2 & 88 \\
        3 & 56, 71, 60, 152, 53 & 4 & 50, 133, 59, 121 \\
        5 & $\emptyset$ & 6 & 82, 29, 43 \\
        7 & 176, 172, 80, 184, 113 & 8 & 109, 87, 173 \\
        9 & 195 & 10 & 91, 111 \\
    \end{tabular}
    \caption{Mejor solución para la instancia 2 (tabla 1)}
\end{table}

\begin{table}[H]
    \centering
    \begin{tabular}{llV{6}ll}
        \textbf{Trabajador} & \textbf{Tareas asignadas} & \textbf{Trabajador} & \textbf{Tareas asignadas}\\
        11 & $\emptyset$ & 12 & 156, 185, 198, 28, 160, 101 \\
        13 & 11, 183, 151 & 14 & 142, 165, 157, 8, 108 \\
        15 & 143, 179 & 16 & $\emptyset$ \\
        17 & 131 & 18 & 83, 167, 84, 114, 86 \\
        19 & 155, 26, 106 & 20 & 34, 116, 14 \\
        21 & 7, 35 & 22 & 163, 182, 4 \\
        23 & 158, 36, 63, 139 & 24 & 19, 119 \\
        25 & 1, 74 & 26 & 38 \\
        27 & 98, 99, 147, 174, 73 & 28 & 153, 54, 81, 100, 197 \\
        29 & 123, 166, 134, 140 & 30 & 20, 42, 125, 169, 68, 78 \\
        31 & 22, 171, 145, 144, 159 & 32 & 30 \\
        33 & $\emptyset$ & 34 & 2, 61 \\
        35 & 193, 67 & 36 & 196, 102, 95 \\
        37 & 120 & 38 & 138, 146, 12, 10 \\
        39 & 16, 97 & 40 & 161, 62, 178 \\
        41 & 15, 90, 89, 17, 77 & 42 & 92, 79, 75, 137 \\
        43 & 110, 40 & 44 & 112 \\
        45 & 25, 127, 180 & 46 & 3, 135 \\
        47 & 129 & 48 & 115, 94 \\
        49 & 46, 122, 107, 66 & 50 & 128, 32, 85 \\
        51 & 48, 57, 13, 103, 149 & 52 & 45 \\
        53 & 9, 136, 150, 58, 148, 44 & 54 & 69, 41 \\
        55 & 72, 37 & 56 & 52 \\
        57 & 188, 47, 126, 31, 130 & 58 & 162, 164 \\
        59 & 191, 49 & 60 & 170, 5, 177 \\
        61 & 132, 104, 6 & 62 & 93, 105, 64 \\
        63 & 124, 186, 76 & 64 & 51, 175, 190, 18 \\
        65 & 187, 200 & 66 & 189, 23, 96 \\
        67 & 21, 154 & 68 & $\emptyset$ \\
        69 & 192, 194 & 70 & 168 \\
        71 & 141, 117, 39 & 72 & 24, 118 \\
        73 & 27, 33, 55 & 74 & $\emptyset$ \\
        75 & 181 \\
    \end{tabular}
    \caption{Mejor solución para la instancia 2 (tabla 2)}
\end{table}

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.4]{instancia2_p1.png}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.3]{instancia2_p2.png}
    \includegraphics[scale=0.3]{instancia2_p3.png}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.3]{instancia2_p4.png}
    \includegraphics[scale=0.3]{instancia2_p5.png}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.3]{instancia2_p6.png}
    \includegraphics[scale=0.3]{instancia2_p7.png}
    \caption{Gráfica bipartita que representa la mejor solución para la instancia 1. La partición izquierda representa a los trabajadores, y la partición derecha representa a las tareas}
\end{figure}

\subsection*{Instancia 3}

\noindent
La instancia tres consta de cincuenta (50) tareas que deben ser asignadas a veinte (20) trabajadores. El mejor resultado encontrado tiene las siguientes características:

\begin{table}[H]
    \centering
    \begin{tabular}{ll}
        \textbf{Factible} & Sí\\
        \textbf{Costo} & 1755.9305\\
        \textbf{Número de soluciones iniciales (n)} & 91\\
        \textbf{Número de soluciones vecinas para mejorar al líder (k)} & 17\\
        \textbf{Número de soluciones compartidas con las soluciones siguientes (x)} & 4\\
        \textbf{Número de tours (m)} & 30\\
        \textbf{Número de iteraciones (K)} & $205*205*205 = 15,000,000$\\
        \textbf{Semilla} & 145\\
        \textbf{Tiempo aproximado de ejecución} & 7 segundos\\
    \end{tabular}
\end{table}

\begin{table}[H]
    \centering
    \begin{tabular}{llV{6}ll}
        \textbf{Trabajador} & \textbf{Tareas asignadas} & \textbf{Trabajador} & \textbf{Tareas asignadas}\\
        1 & 45, 11 & 2 & 40, 24, 39 \\
        3 & 25 & 4 & 44, 37 \\
        5 & 13, 1, 31 & 6 & 21 \\
        7 & 28, 19, 48 & 8 & 16, 33 \\
        9 & 3 & 10 & 38, 18, 50, 32 \\
        11 & 47 & 12 & 30, 12, 27, 43 \\
        13 & 9 & 14 & 5, 46, 49, 41, 35 \\
        15 & 8, 4, 17, 6, 20 & 16 & 2, 29 \\
        17 & 10, 22 & 18 & $\emptyset$ \\
        19 & 26, 7, 42, 15, 23 & 20 & 14, 36, 34 \\
    \end{tabular}
    \caption{Mejor solución para la instancia 3 (tabla)}
\end{table}

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.3]{instancia3_p1.png}
    \includegraphics[scale=0.3]{instancia3_p2.png}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.3]{instancia3_p3.png}
    \caption{Gráfica bipartita que representa la mejor solución para la instancia 3. La partición izquierda representa a los trabajadores, y la partición derecha representa a las tareas}
\end{figure}


\subsection*{Instancia 4}

\noindent
La instancia cuatro consta de cien (100) tareas que deben ser asignadas a cuarenta (40) trabajadores. El mejor resultado encontrado tiene las siguientes características:

\begin{table}[H]
    \centering
    \begin{tabular}{ll}
        \textbf{Factible} & Sí\\
        \textbf{Costo} & 2288.82\\
        \textbf{Número de soluciones iniciales (n)} & 91\\
        \textbf{Número de soluciones vecinas para mejorar al líder (k)} & 13\\
        \textbf{Número de soluciones compartidas con las soluciones siguientes (x)} & 4\\
        \textbf{Número de tours (m)} & 30\\
        \textbf{Número de iteraciones (K)} & $190^2 * 187 = 6,750,700$\\
        \textbf{Semilla} & 16\\
        \textbf{Tiempo aproximado de ejecución} & 9 segundos\\
    \end{tabular}
\end{table}

\begin{table}[H]
    \centering
    \begin{tabular}{llV{6}ll}
        \textbf{Trabajador} & \textbf{Tareas asignadas} & \textbf{Trabajador} & \textbf{Tareas asignadas}\\
        1 & 76, 25 & 2 & 71, 54, 65, 29 \\
        3 & 82 & 4 & 4, 51 \\
        5 & 37, 96 & 6 & 94, 83, 20, 9, 10, 85 \\
        7 & 95, 53, 47 & 8 & 35, 18, 73 \\
        9 & $\emptyset$ & 10 & 79 \\
        11 & 49, 17, 93, 38, 59 & 12 & 45, 28, 58 \\
        13 & 78 & 14 & 80, 44, 16, 84 \\
        15 & 14 & 16 & 63, 50, 92, 64 \\
        17 & 56, 43 & 18 & 75, 68, 5 \\
        19 & 88, 62, 48, 3, 23, 87, 100 & 20 & 57, 21, 91, 41 \\
        21 & 97, 66, 86, 34, 31, 74 & 22 & 24 \\
        23 & 6, 40 & 24 & 36 \\
        25 & 61, 72 & 26 & $\emptyset$ \\
        27 & 33 & 28 & $\emptyset$ \\
        29 & 1, 13, 11, 42 & 30 & 19, 32 \\
        31 & 12, 60 & 32 & 77, 99, 70 \\
        33 & 69, 81, 98, 2 & 34 & 55, 7, 46, 27 \\
        35 & 67 & 36 & 8 \\
        37 & 30 & 38 & 89, 22, 15 \\
        39 & 39, 90 & 40 & 26, 52 \\
    \end{tabular}
    \caption{Mejor solución para la instancia 4 (tabla)}
\end{table}

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.3]{instancia4_p1.png}
    \includegraphics[scale=0.3]{instancia4_p2.png}
    \includegraphics[scale=0.3]{instancia4_p3.png}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.3]{instancia4_p4.png}
    \includegraphics[scale=0.3]{instancia4_p5.png}
    \caption{Gráfica bipartita que representa la mejor solución para la instancia 4. La partición izquierda representa a los trabajadores, y la partición derecha representa a las tareas}
\end{figure}

\newpage

\begin{thebibliography}{9}
\bibitem{GAP}
Kundakcioglu, O. E., \& Alizamir, S. (2008). Generalized Assignment Problem. Encyclopedia of Optimization, 7, 1153–1162. https://doi.org/10.1007/978-0-387-74759-0\_200

\bibitem{MINGAP}
Martello, S., \& Toth, P. (1990). Knapsack Problems: Algorithms and Computer Implementations (Wiley Series in Discrete Mathematics and Optimization) (1st ed.). Wiley.

\bibitem{MBO}
Duman, E., Uysal, M., \& Alkaya, A. F. (2012). Migrating Birds Optimization: A new metaheuristic approach and its performance on quadratic assignment problem. Information Sciences, 217, 65–77. https://doi.org/10.1016/j.ins.2012.06.032

\bibitem{MBO_extra}
Tongur, V., Ertunc, E., \& Uyan, M. (2020a). Use of the Migrating Birds Optimization (MBO) Algorithm in solving land distribution problem. Land Use Policy, 94, 104550. https://doi.org/10.1016/j.landusepol.2020.104550


\end{thebibliography}
 
\end{document}
