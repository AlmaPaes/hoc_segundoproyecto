use rand::prelude::*;
use std::fmt;
use crate::data::*;
use std::rc::Rc;

#[derive(Clone)]
pub struct Solucion{
    datos: Rc<Datos>,
    pub asignacion: Vec<Vec<i32>>,
    pub capacidades: Vec<f64>,
    pub costo: f64,
    pub penalizacion: f64
}

fn costo_inicial(asignacion: &Vec<Vec<i32>>, costos: &Vec<Vec<f64>>) -> f64 {
    let mut costo = 0f64;
    let mut t = 1;
    for trab in asignacion.iter(){
        for tarea in trab.iter(){
            costo += costos[(*tarea-1) as usize][t-1];
        }
        t += 1;
    }
    costo
}

fn capacidades_iniciales(asignacion: &Vec<Vec<i32>>, caps: &Vec<Vec<f64>>, trabajadores: &Vec<f64>) -> (Vec<f64>, f64){
    let mut capacidades: Vec<f64> = Vec::with_capacity(asignacion.len());
    let mut suma;
    let mut i = 1;
    let mut penalizacion = 0f64;
    for trab in asignacion.iter(){
        suma = trab.iter().fold(0f64, |sum,x| sum + caps[(*x-1) as usize][(i-1) as usize]);
        if suma > trabajadores[(i-1) as usize]{
            penalizacion += 1f64;
        }
        capacidades.push(suma);
        i += 1;
    }
    (capacidades,penalizacion)
}

impl Solucion{
    pub fn new(datos: Rc<Datos>, asignacion: Vec<Vec<i32>>, capacidad_actual: Vec<f64>, costo: f64, penalizacion: f64) -> Solucion{
        let costo_real = 
        if costo == 0f64 {
            costo_inicial(&asignacion, &datos.costos)
        } else {
            costo
        };

        let (capacidades, penal) = 
        if capacidad_actual.len() == 0{
            capacidades_iniciales(&asignacion, &datos.capacidades, &datos.trabajadores)
        } else {
            (capacidad_actual, penalizacion)
        };

        Solucion {
            datos,
            asignacion,
            capacidades: capacidades,
            costo: costo_real*(penal+1f64),
            penalizacion: penal
        }
    }

    fn get_new_index(&self, rng: &mut rand_chacha::ChaCha8Rng, different1: i32) -> i32{
        let mut new_index;
        let index = loop{
            new_index = rng.gen_range(0, (self.asignacion.len()) as i32);
            if different1 == -1 {
                if self.asignacion[new_index as usize].len() != 0 {
                    break new_index;
                }
            }
            else{
                if new_index != different1 && !self.sobrepasa(new_index){
                    break new_index; 
                }
            }
        };

        index
        
    }

    pub fn get_vecino(&self, rng: &mut rand_chacha::ChaCha8Rng) -> Solucion{
        let index1 = self.get_new_index(rng, -1);
        let index2 = self.get_new_index(rng, index1);
        let mut nueva_asignacion = self.asignacion.clone();
        let tarea_movida = nueva_asignacion[index1 as usize].pop().unwrap();
        let nuevo_indice = nueva_asignacion[index2 as usize]
                            .binary_search_by(|&a| self.datos.capacidades[(a-1) as usize][index2 as usize]
                            .partial_cmp(&self.datos.capacidades[(tarea_movida-1) as usize][index2 as usize]).unwrap())
                            .unwrap_or_else(|x| x);
        nueva_asignacion[index2 as usize].insert(nuevo_indice,tarea_movida); 
        let nueva_cap = self.actualizar_capac(tarea_movida, index1, index2);
        let nuevo_costo = self.actualizar_costo(tarea_movida, index1, index2);
        let nueva_penal = self.actualizar_penalizacion(index1, index2, &nueva_cap);
        Solucion::new(Rc::clone(&self.datos),nueva_asignacion, nueva_cap, nuevo_costo, nueva_penal)
    }

    fn actualizar_capac(&self, tarea: i32, index1: i32, index2: i32) -> Vec<f64>{
            let mut nuevas_cap = self.capacidades.clone();
            nuevas_cap[index1 as usize] -= self.datos.capacidades[(tarea-1) as usize][index1 as usize];
            nuevas_cap[index2 as usize] += self.datos.capacidades[(tarea-1) as usize][index2 as usize];
            nuevas_cap
        }

    fn actualizar_penalizacion(&self, index1: i32, index2: i32, nuevas_caps: &Vec<f64>) -> f64{
        let mut nueva = self.penalizacion;
        if self.sobrepasa(index1) {
            nueva -= 1f64;
        }
        if self.sobrepasa(index2) {
            nueva -= 1f64;
        }
        if nuevas_caps[index1 as usize] > self.datos.trabajadores[index1 as usize]{
            nueva += 1f64;
        }
        if nuevas_caps[index2 as usize] > self.datos.trabajadores[index2 as usize]{
            nueva += 1f64;
        }
        nueva
    }

    fn sobrepasa(&self, trab: i32) -> bool{
        self.capacidades[trab as usize] > self.datos.trabajadores[trab as usize]
    }

 
    fn actualizar_costo(&self, tarea: i32, index1: i32, index2: i32) -> f64 {
        let mut costo_actual = self.costo / (self.penalizacion+1f64);
        costo_actual -= self.datos.costos[(tarea-1) as usize][index1 as usize];
        costo_actual += self.datos.costos[(tarea-1) as usize][index2 as usize];
        costo_actual
    }

}

impl fmt::Debug for Solucion{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_list().entries(self.asignacion.iter()).finish()
    }
}