extern crate rusqlite;
use std::path::Path;
//use self::rusqlite::Connection;
use rusqlite::{Connection, NO_PARAMS};


struct Capacidad {
    id_tarea: i32,
    id_trabajador: i32,
    capacidad: f64
}

struct Costo {
    id_tarea: i32,
    id_trabajador: i32,
    costo: f64
}

pub fn db_connection(db: String) -> Connection {
    let db_path = Path::new(&db);
    let connection = Connection::open(db_path).unwrap();
    //connection.execute(&db, NO_PARAMS).unwrap();
    connection
}

pub fn get_tareas(conn: &Connection) -> Vec<i32> {
    let mut stmt = conn.prepare("SELECT id from tareas")
                       .expect("Error al preparar conexión para obtener distancias entre ciudades.");
    let ids = stmt.query_map(NO_PARAMS, |row| { row.get(0)}).unwrap();
    let mut tareas = Vec::new();
    for id in ids{
        tareas.push(id.unwrap());
    }
    tareas
}

pub fn get_trabajadores(conn: &Connection) -> Vec<f64> {
    let mut stmt = conn.prepare("SELECT capacidad from trabajadores")
                       .expect("Error al preparar conexión para obtener distancias entre ciudades.");
    let caps = stmt.query_map(NO_PARAMS, |row| { row.get(0)}).unwrap();
    let mut trabajadores = Vec::new();
    for cap in caps{
        trabajadores.push(cap.unwrap());
    }
    trabajadores
}

pub fn get_capacidades(conn: &Connection, tareas: usize, trab: usize) -> Vec<Vec<f64>> {
    let mut stmt = conn.prepare("SELECT * from capacidades")
                       .expect("Error al preparar conexión para obtener distancias entre ciudades.");
    
    let capacidades = stmt.query_map(NO_PARAMS, |row| {
        Ok(Capacidad{
            id_tarea: row.get(0)?,
            id_trabajador: row.get(1)?,
            capacidad: row.get(2)?
        })
    }).unwrap();

    let mut capacidades_mat: Vec<Vec<f64>> = vec![vec![0f64; trab]; tareas];
    
    for cap in capacidades{
        let capacidad = cap.unwrap();
        capacidades_mat[(capacidad.id_tarea-1) as usize][(capacidad.id_trabajador-1) as usize] = capacidad.capacidad; 
    }
    capacidades_mat
    
}

pub fn get_costos(conn: &Connection, tareas: usize, trab: usize) -> Vec<Vec<f64>> {
    let mut stmt = conn.prepare("SELECT * from costos")
                       .expect("Error al preparar conexión para obtener distancias entre ciudades.");
    
    let costos_extract = stmt.query_map(NO_PARAMS, |row| {
        Ok(Costo{
            id_tarea: row.get(0)?,
            id_trabajador: row.get(1)?,
            costo: row.get(2)?
        })
    }).unwrap();

    let mut costos: Vec<Vec<f64>> =  vec![vec![0f64; trab]; tareas];
    for c in costos_extract{
        let costo= c.unwrap();
        costos[(costo.id_tarea-1) as usize][(costo.id_trabajador-1) as usize] = costo.costo; 
    }
    costos
}