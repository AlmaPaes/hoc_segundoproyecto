mod database;
mod solucion;
mod migracion_aves;
mod data;


use crate::migracion_aves::*;
use crate::data::*;
use crate::solucion::*;
use std::time::{Instant};
use std::env;
use std::io;
use std::process;
use std::sync::{Arc, Mutex};
use threadpool::ThreadPool;

fn make_cute_string(solucion: &Solucion) -> String{
    let mut cadena = String::new();
    let asignacion = &solucion.asignacion;
    let mut num_trabajador = 0;
    for t in asignacion{
        num_trabajador += 1;
        cadena.push('W');
        cadena.push_str(&num_trabajador.to_string());
        cadena.push_str(": ");
        for tarea in t{
            cadena.push('T');
            cadena.push_str(&tarea.to_string());
            cadena.push_str(", ");
        }
        //cadena = cadena.strip_suffix(", ").unwrap().to_string();
        cadena.push('\n');
    }

    cadena = cadena.trim_end_matches('\n').to_string();
    cadena
}

fn read_instance() -> String{
    let mut base = String::new();
    match io::stdin().read_line(&mut base){
        Ok(_) => {}
        Err(_) => println!("Problema con recibir el nombre del archivo"),
    };

    let x: &[_] = &['\r', '\n'];
    let cadena = base.trim_end_matches(x);
    
    String::from(cadena)
}

fn get_argumentos(args: Vec<String>) -> Result<Vec<u64>, &'static str>{
    if args.len() < 2{
        return Err("Argumentos incorrectos: {semillas}");
    }

    let mut semillas: Vec<u64> = Vec::new();
    for a in args.split_first().unwrap().1.iter(){
        match a.parse::<i32>(){
            Ok(seed) => semillas.push(seed as u64),
            Err(_) => {
                    println!("Semilla incorrecta. Sólo son aceptados números enteros");
                    process::exit(1);
            }
        }
    }
    
    Ok(semillas)

}

fn run_heuristica(semilla: u64, datos: Datos){
    println!("hola desde la semilla {}", semilla);
    let n = 91;
    let k = 17;
    let x = 4;
    let m = 30;
    let iteraciones = 205*205*205;
    let mut formacion = Formacion::new(n,k,x,m,iteraciones,semilla, datos);
    let solucion = formacion.migracion_aves();
    println!("{}", make_cute_string(&solucion));
    //println!("{:?}", solucion);
    println!("{:.64}", solucion.costo);
    println!("penalizaciones: {}", solucion.penalizacion);

}

fn main() {

    let args: Vec<String> = env::args().collect();
    let semillas: Vec<u64>;
    match get_argumentos(args){
        Ok(datos) => semillas = datos,
        Err(e) => {
            println!("{}",e);
            process::exit(1);
        }
    }

    let input = read_instance();
    //println!("{}", input);

    //let now = Instant::now(); 
    let datos = Arc::new(Mutex::new(Datos::new(input)));

    let pool = ThreadPool::new(10);

    for seed in semillas.into_iter(){
        let mis_datos = Arc::clone(&datos);
        pool.execute(move || {
            run_heuristica(seed, mis_datos.lock().unwrap().clone());
        });
    }
    pool.join();
    
    //let new_now = Instant::now();
    //println!("{:?}", new_now.duration_since(now));
}
