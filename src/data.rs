//mod database;
use crate::database::*;

#[derive(Clone)]
pub struct Datos{
    pub tareas: Vec<i32>,
    pub trabajadores: Vec<f64>,
    pub capacidades: Vec<Vec<f64>>,
    pub costos: Vec<Vec<f64>>
}

impl Datos{
    pub fn new(db: String) -> Datos {
        let connection = db_connection(db);
        let tareas = get_tareas(&connection);
        let trabajadores = get_trabajadores(&connection);
        let num_tareas = tareas.len();
        let num_trab = trabajadores.len();
        Datos {
            tareas: tareas,
            trabajadores: trabajadores,
            capacidades: get_capacidades(&connection, num_tareas, num_trab),
            costos: get_costos(&connection, num_tareas, num_trab)
        }
    }

}