use crate::data::*;
use crate::solucion::*;
use rand::prelude::*;
use rand_chacha::ChaCha8Rng;
use std::rc::Rc;


pub struct Formacion {
    num_aves: i32,     //n
    sols_vecinas: i32, //k
    sols_mejores: i32, //x
    num_tours: i32,    //m
    iteraciones: i32,  //K
    rnd_generator: rand_chacha::ChaCha8Rng,
    datos: Rc<Datos>,
}

impl Formacion {
    pub fn new(
        num_aves: i32,
        sols_vecinas: i32,
        sols_mejores: i32,
        num_tours: i32,
        iteraciones: i32,
        semilla: u64,
        datos: Datos,
    ) -> Formacion {
        let rng = <ChaCha8Rng as SeedableRng>::seed_from_u64(semilla);
        Formacion {
            num_aves,
            sols_vecinas,
            sols_mejores,
            num_tours,
            iteraciones,
            rnd_generator: rng,
            datos: Rc::new(datos),
        }
    }

    fn get_random_solution(&mut self) -> Vec<Vec<i32>> {
        let mut solucion = vec![vec![]; self.datos.trabajadores.len()];
        let mut trab;
        for i in 1..=self.datos.tareas.len(){
            trab = self.rnd_generator.gen_range(1, self.datos.trabajadores.len() + 1);
            let cap_actual = self.datos.capacidades[i - 1][trab - 1];
            let index = solucion[trab - 1]
                .binary_search_by(|&a| {
                    self.datos.capacidades[(a - 1) as usize][trab - 1]
                        .partial_cmp(&cap_actual)
                        .unwrap()
                })
                .unwrap_or_else(|x| x);
            solucion[trab - 1].insert(index, i as i32);
        }
        solucion
    }

    pub fn migracion_aves(&mut self) -> Solucion {
        let mut lider = Solucion::new(
            Rc::clone(&self.datos),
            self.get_random_solution(),
            Vec::new(),
            0f64,
            0f64,
        );
        let mut soldados_i: Vec<Solucion> = Vec::new();
        let mut soldados_d: Vec<Solucion> = Vec::new();
        let mut soldado_izq;
        let mut soldado_der;
        for _i in 0..(self.num_aves - 1)/2 {
            soldado_izq = Solucion::new(
                Rc::clone(&self.datos),
                self.get_random_solution(),
                Vec::new(),
                0f64,
                0f64,
            );
            soldados_i.push(soldado_izq);

            soldado_der = Solucion::new(
                Rc::clone(&self.datos),
                self.get_random_solution(),
                Vec::new(),
                0f64,
                0f64,
            );
            soldados_d.push(soldado_der);
        }

        let mut mejor_solucion = lider.clone();

        let mut it = 0;


        while it < self.iteraciones {
            for _j in 0..self.num_tours {
                let mut nueva: Solucion;
                let mut anteriores_sin_usar: Vec<Solucion> = Vec::new();
                let mut index_s;
                for _k in 0..self.sols_vecinas {
                    nueva = lider.get_vecino(&mut self.rnd_generator);
                    index_s = anteriores_sin_usar
                        .binary_search_by(|a| a.costo.partial_cmp(&nueva.costo).unwrap())
                        .unwrap_or_else(|x| x);
                    anteriores_sin_usar.insert(index_s, nueva);
                }

                if anteriores_sin_usar[0].costo < lider.costo {
                    lider = anteriores_sin_usar.remove(0);
                }

                let mut anteriores_der = anteriores_sin_usar.split_off((anteriores_sin_usar.len() as f64 / 2f64).floor() as usize);

                it += self.sols_vecinas;

                for sol in soldados_i.iter_mut() {
                    let mut vecinos: Vec<Solucion> = Vec::new();
                    let mut index_si;
                    for _n in 0..(self.sols_vecinas - self.sols_mejores) {
                        nueva = sol.get_vecino(&mut self.rnd_generator);
                        index_si = vecinos
                            .binary_search_by(|a| a.costo.partial_cmp(&nueva.costo).unwrap())
                            .unwrap_or_else(|x| x);
                        vecinos.insert(index_si, nueva);
                    } //endfor k-x mejoramientos

                    if vecinos[0].costo < sol.costo {
                        *sol = vecinos.remove(0);
                    }


                    if anteriores_sin_usar[0].costo < sol.costo{
                        *sol = anteriores_sin_usar.remove(0);
                    }

                    anteriores_sin_usar = vecinos;

                    it += self.sols_vecinas - self.sols_mejores;
                } //endfor mejorar soluciones soldados_izq


                for sol in soldados_d.iter_mut() {
                    let mut vecinos: Vec<Solucion> = Vec::new();
                    let mut index_sd;
                    for _n in 0..(self.sols_vecinas - self.sols_mejores) {
                        nueva = sol.get_vecino(&mut self.rnd_generator);
                        index_sd = vecinos
                            .binary_search_by(|a| a.costo.partial_cmp(&nueva.costo).unwrap())
                            .unwrap_or_else(|x| x);
                        vecinos.insert(index_sd, nueva);
                    } //endfor k-x mejoramientos

                    if vecinos[0].costo < sol.costo {
                        *sol = vecinos.remove(0);
                    }

                    if anteriores_der[0].costo < sol.costo{
                        *sol = anteriores_der.remove(0);
                    }

                    anteriores_der = vecinos;

                    it += self.sols_vecinas - self.sols_mejores;
                } //endfor mejorar soluciones soldados_der


            } //endfor tours

            if lider.costo < mejor_solucion.costo {
                mejor_solucion = lider.clone();
            }

            soldados_d.push(lider.clone());
            lider = soldados_i.remove(0);
            soldados_i.push(soldados_d.remove(0));
            
        } //endwhile iteracion

        mejor_solucion
    }
}
