# HOC_PrimerProyecto

Seminario de Heurísticas y Optimización Combinatoria 2021-1\
Proyecto 2\
:construction_worker: Generalized Assignment Problem con Migrating Birds Optimization :memo:

## Autora
Alma Rosa Páes Alcalá :beetle:

---

## Versiones de software
* **rustc** `1.47.0`
* **cargo** `1.47.0`
* Versiones de dependencias utilizadas se encuentran en el archivo [**Cargo.toml**](Cargo.toml)

# Requerimientos

La instancia debe estar almacenada en una base de datos, la cual debió ser creada con SQLite.


## Modo de uso
**Para compilar:** `cargo build`\
**Para correr el programa:** 
`cargo run --release {semilla} <<< {base de datos en formato db}`
